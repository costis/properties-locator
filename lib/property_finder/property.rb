module PropertyFinder

  class Property
    attr_reader :name, :bedroom_count, :location

    def initialize(name, bedcount, location)
      @name, @bedroom_count, @location = name, bedcount, location
    end
  end
end
