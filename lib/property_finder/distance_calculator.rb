module PropertyFinder
  class DistanceCalculator
    RAD_PER_DEG   = 0.017453293
    RADIUS_IN_KM  = 6371

    # Implementation taken from http://www.esawdust.com (Landon Cox)
    def self.calc_distance(point_a, point_b)
      dlon = point_b.lon - point_a.lon
      dlat = point_b.lat - point_a.lat

      dlon_rad = dlon * RAD_PER_DEG
      dlat_rad = dlat * RAD_PER_DEG

      lat1_rad = point_a.lat * RAD_PER_DEG
      lon1_rad = point_a.lon * RAD_PER_DEG

      lat2_rad = point_b.lat * RAD_PER_DEG
      lon2_rad = point_b.lat * RAD_PER_DEG
      # puts "dlon: #{dlon}, dlon_rad: #{dlon_rad}, dlat: #{dlat}, dlat_rad: #{dlat_rad}"

      a = (Math.sin(dlat_rad/2))**2 + Math.cos(lat1_rad) * Math.cos(lat2_rad) * (Math.sin(dlon_rad/2))**2
      c = 2 * Math.atan2( Math.sqrt(a), Math.sqrt(1-a))

      distance_in_km = RADIUS_IN_KM * c
    end
  end
end
