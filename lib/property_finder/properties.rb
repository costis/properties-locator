require 'forwardable'

module PropertyFinder
  class Properties
    extend Forwardable
    include Enumerable

    attr_reader :properties
    def_delegators :@properties, :each, :size

    def initialize(distance_calculator = PropertyFinder::DistanceCalculator)
      @properties = []
      @distance_calculator = distance_calculator
    end

    def add(property)
      @properties += [property].flatten
    end

    def find_similar(property)
      similar_properties = self.inject({}) do |acc, current_property|
        distance = calculate_distance(property, current_property)
        acc[current_property] = distance if matches_criteria?(distance, property, current_property)
        acc
      end

      sort_properties_by_distance(similar_properties)
    end

    private
    def matches_criteria?(distance, prop_a, prop_b)
      return false if prop_a == prop_b
      required_bedcount?(prop_a.bedroom_count, prop_b.bedroom_count) && required_distance?(distance)
    end

    def required_distance?(distance)
      distance <= 20
    end

    def required_bedcount?(required, actual)
      actual >= required
    end

    def calculate_distance(prop_a, prop_b)
      @distance_calculator.calc_distance(prop_a.location, prop_b.location)
    end

    def sort_properties_by_distance(properties)
      properties.sort_by {|k,v| v}.map{ |s| s.first }
    end
  end
end
