require 'spec_helper'

describe PropertyFinder::DistanceCalculator do
  let(:point_a) { PropertyFinder::Location.new(39.06546, -104.88544) }
  let(:point_b) { PropertyFinder::Location.new(39.06546, -104.80) }

  describe ".calc_distance" do
    it "should calculate the distance between two points" do
      subject.class.calc_distance(point_a, point_b).should be_within(0.1).of(7.37)
    end
  end
end
