require 'spec_helper'

describe PropertyFinder::Properties do
  context "adding properties " do
    let(:property_a) { mock('property a') }
    let(:property_b) { mock('property b') }
    let(:property_c) { mock('property c') }
    let(:property_d) { mock('property d') }

    it "can add many properties at once" do
      subject.add([property_c, property_d])
      subject.properties.should include(property_c, property_d)
    end

    it "exposes properties" do
      subject.add(property_a)
      subject.add(property_b)
      subject.properties.should include(property_a, property_b)
    end
  end

  context "acts as enumberable" do
    it "has a #each method" do
      subject.should respond_to(:each)
    end

    it "has a #size method" do
      subject.should respond_to(:size)
    end
  end

  context "searching for properties" do
    it "has a method for finding similar properties" do
      subject.should respond_to(:find_similar)
    end

    describe "locating similar properties" do
      let(:p0) { PropertyFinder::Property.new('reference property', 2, PropertyFinder::Location.new(0,0)) }
      let(:p1) { PropertyFinder::Property.new('within 20KM, higher bedcount', 3, PropertyFinder::Location.new(0,0.022)) }
      let(:p2) { PropertyFinder::Property.new('within 20KM, same bedcount', 2, PropertyFinder::Location.new(0,0.033)) }
      let(:p3) { PropertyFinder::Property.new('within 20KM, same bedcount', 2, PropertyFinder::Location.new(0,0.011)) }
      let(:p4) { PropertyFinder::Property.new('within 20KM, less bedcount', 1, PropertyFinder::Location.new(0,0.022)) }

      let(:p5) { PropertyFinder::Property.new('over 20KM, higher bedcount', 3, PropertyFinder::Location.new(0,0.180)) }
      let(:p6) { PropertyFinder::Property.new('over 20KM, same bedcount', 2, PropertyFinder::Location.new(0,0.182)) }
      let(:p7) { PropertyFinder::Property.new('over 20KM, less bedcount', 1, PropertyFinder::Location.new(0,0.190)) }

      before(:each) do
        subject.add([p0,p1,p2,p3,p4,p5,p6])
      end

      it "locates properties which are no more than 20 km away and have same or higher number of beds" do
        res = subject.find_similar(p0)
        res.should include(p1,p2,p3)
        res.should_not include(p4)
      end

      it "returned properties are shorted by distance" do
        res = subject.find_similar(p0)
        res.should == [p3, p1, p2]
      end

      it "does not included the searched property" do
        res = subject.find_similar(p0)
        res.should_not include(p0)
      end
    end
  end
end
