require 'spec_helper'

describe PropertyFinder::Property do
  let(:location) { mock('location') }
  subject { PropertyFinder::Property.new('a name', 5, location) }

  describe "#initialize" do
    it "should initialize with correct values" do
      subject.name.should eq 'a name'
      subject.bedroom_count.should eq 5
      subject.location.should eq location
    end
  end

  describe "public interface" do
    it "has a 'name' property" do
      subject.should respond_to(:name)
    end

    it "has a 'bedroom_count' property" do
      subject.should respond_to(:bedroom_count)
    end

    it "has a 'location' property" do
      subject.should respond_to(:location)
    end
  end
end
